const Redis = require("ioredis");
const puppeteer = require('puppeteer');
const fs = require("fs");

const USERNAME = 'Cust1@agilecode.io';
const PASSWORD = 'Pa55word1234';
const USERNAME2 = 'Cust2@agilecode.io';
const PASSWORD2 = 'Pa55word1234';
const LOGIN_PAGE = 'http://devtt.agilecode.io/dev/login';

const FIRST_20_LAYOUT_FILENAME = "first.txt"
const LAST_20_LAYOUT_FILENAME = "last.txt"

const REDIS_HOST = "127.0.0.1"
const REDIS_PORT = 6379
const REDIS_PASSWORD = "password"
const REDIS_DB = 2
const PAGES = 10

const redis = new Redis({
    port: REDIS_PORT,
    host: REDIS_HOST ,
    family: 4,
    password: REDIS_PASSWORD,
    db: REDIS_DB
});

const getFileData = file_name => {
    return new Promise(function(resolve, reject) {
        fs.readFile(file_name, function(err, buf) {
            if (err) {
                reject(err)
            } else {
                resolve (buf.toString())
            }
        });
    });
}

// redis.get("cust1_otp/profile", function(err, profile) {
//     fs.writeFileSync("last.txt", profile, (err) => {
//         if (err) console.log(err);
//         console.log("Successfully Written to File.");
//     });
// });

(async () => {
    const browser = await puppeteer.launch({
        args: [
            '--disable-background-timer-throttling',
            '--disable-renderer-backgrounding',
            '--override-plugin-power-saver-for-testing=never',
            '--disable-extensions-http-throttling',
        ],
        headless: false
    })
    
    const first_20 = await getFileData(FIRST_20_LAYOUT_FILENAME)
    const last_20 = await getFileData(LAST_20_LAYOUT_FILENAME)

    await redis.set("cust1_otp/profile", first_20);
    await redis.set("cust2_otp/profile", last_20);

    for(let i = 1; i <= PAGES; i++){
        const context = await browser.createIncognitoBrowserContext();

        const page = await context.newPage();

        let target_username = USERNAME2;
        let target_password = PASSWORD2;
        if (i <= Math.round(PAGES * 0.5)) {
            target_username = USERNAME;
            target_password = PASSWORD;
        }
        console.log(`Tab #${i} -> Starting`)

        await page.goto(LOGIN_PAGE, {waitUntil: 'networkidle2'});

        await page.focus('[name=login]')
        await page.keyboard.type(target_username)

        await page.focus('[name=password]')
        await page.keyboard.type(target_password)

        await page.keyboard.press('Enter');

        await page.waitForNavigation({waitUntil: 'domcontentloaded'})

        console.log(`Tab #${i} -> Done`)
    }
})();